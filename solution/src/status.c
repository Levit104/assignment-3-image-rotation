#include "status.h"

const char *status_message[] = {
        [SUCCESS] = "Изображение было успешно повернуто!",
        [INVALID_ARGS] = "Недопустимое кол-во аргументов!",
        [ROTATION_ERROR] = "Ошибка при повороте изображения!",

        [IO_OPEN_OK] = "Файл успешно открыт",
        [IO_OPEN_ERROR] = "Ошибка при открытии файла!",

        [IO_CLOSE_OK] = "Файл успешно закрыт",
        [IO_CLOSE_ERROR] = "Ошибка при закрытии файла!",

        [READ_OK] = "Успешное чтение файла",
        [READ_OPEN_ERROR] = "Ошибка при открытии файла для чтения!",
        [READ_INVALID_SIGNATURE] = "Некорректная сигнатура!",
        [READ_INVALID_OFF_BITS] = "Некорректное смещение относительно начала файла!",
        [READ_INVALID_INFO_SIZE] = "Некорректный размер заголовка",
        [READ_INVALID_WIDTH_HEIGHT] = "Некорректные ширина и/или высота!",
        [READ_INVALID_PLANES] = "Некорректное кол-во плоскостей!",
        [READ_INVALID_BITS] = "Некорректное кол-во бит на пиксель!",
        [READ_INVALID_COMPRESSION] = "Некорректный тип сжатия!",
        [READ_INVALID_COLORS] = "Некорректное кол-во используемых цветов!",
        [READ_NULL_ERROR] = "Ошибка при считывании данных файла!",
        [READ_PIXELS_ERROR] = "Ошибка при считывании пикселей!",

        [WRITE_OK] = "Успешная запись в файл",
        [WRITE_ERROR] = "Ошибка при записи в файл!"
};
