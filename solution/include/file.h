#ifndef IMAGE_TRANSFORMER_FILE_H
#define IMAGE_TRANSFORMER_FILE_H

#include "status.h"
#include <stdio.h>

enum status open_file(const char *filename, FILE **stream, const char *mode);

enum status close_file(FILE *stream);

#endif //IMAGE_TRANSFORMER_FILE_H
