#ifndef IMAGE_TRANSFORMER_ROTATION_H
#define IMAGE_TRANSFORMER_ROTATION_H

struct image rotate_image(struct image source);

#endif //IMAGE_TRANSFORMER_ROTATION_H
