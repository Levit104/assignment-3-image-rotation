#include "bmp.h"
#include "file.h"
#include "rotation.h"
#include "util.h"

int main(int argc, char **argv) {
    enum status status;

    if (argc != 3) {
        status = INVALID_ARGS;
        println_error(status_message[status]);
        return status;
    }

    FILE *input_stream = NULL;
    FILE *output_stream = NULL;
    const char *input_filename = argv[1];
    const char *output_filename = argv[2];
    struct image original_image = {0};
    struct image rotated_image = {0};


    status = open_file(input_filename, &input_stream, "rb");
    if (status != IO_OPEN_OK) {
        println_error(status_message[status]);
        return status;
    }

    status = from_bmp(input_stream, &original_image);
    if (status != READ_OK) {
        if (status != READ_OPEN_ERROR && status != READ_NULL_ERROR && status != READ_PIXELS_ERROR) {
            println_error("Неподдерживаемый файл bmp!");
        }
        println_error(status_message[status]);
        free_image(original_image);
        close_file(input_stream);
        return status;
    }

    status = close_file(input_stream);
    if (status != IO_CLOSE_OK) {
        println_error(status_message[status]);
        free_image(original_image);
        return status;
    }

    rotated_image = rotate_image(original_image);
    free_image(original_image);

    if (rotated_image.data == NULL) {
        status = ROTATION_ERROR;
        println_error(status_message[status]);
        return status;
    }

    status = open_file(output_filename, &output_stream, "wb");
    if (status != IO_OPEN_OK) {
        println_error(status_message[status]);
        free_image(rotated_image);
        return status;
    }

    status = to_bmp(output_stream, &rotated_image);
    if (status != WRITE_OK) {
        println_error(status_message[status]);
        free_image(rotated_image);
        close_file(output_stream);
        return status;
    }

    free_image(rotated_image);

    status = close_file(output_stream);
    if (status != IO_CLOSE_OK) {
        println_error(status_message[status]);
        return status;
    }

    status = SUCCESS;
    println(status_message[status]);
    return status;
}
