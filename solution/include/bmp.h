#ifndef IMAGE_TRANSFORMER_BMP_H
#define IMAGE_TRANSFORMER_BMP_H

#include "image.h"
#include "status.h"
#include <stdio.h>

struct __attribute__((packed)) bmp_header;

enum status from_bmp(FILE *stream, struct image *image);

enum status to_bmp(FILE *stream, const struct image *image);

#endif //IMAGE_TRANSFORMER_BMP_H
