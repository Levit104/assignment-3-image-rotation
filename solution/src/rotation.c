#include "image.h"
#include "rotation.h"
#include <stddef.h>

struct image rotate_image(struct image source) {
    const uint64_t width = source.width;
    const uint64_t height = source.height;
    struct image new_image = create_image(height, width);
    for (size_t i = 0; i < width; i++) {
        for (size_t j = 0; j < height; j++) {
            new_image.data[j + i * new_image.width] = source.data[i + (height - j - 1) * width];
        }
    }
    return new_image;
}
