#include "bmp.h"

struct __attribute__((packed)) bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};

/*
 * http://aco.ifmo.ru/el_books/image_processing/3_06.html
 * https://ru.wikipedia.org/wiki/BMP
 * https://stackoverflow.com/questions/25713117/what-is-the-difference-between-bisizeimage-bisize-and-bfsize
 * https://jenyay.net/Programming/Bmp
*/
enum {
    HEADER_SIZE = sizeof(struct bmp_header),
    SIGNATURE = 0x4D42,
    OFF_BITS = 54,
    INFO_SIZE = 40,
    PLANES = 1,
    BITS = 24,
    COMPRESSION = 0,
    PIX_PER_METER = 2834, // ???
    ALL_COLORS = 0,
    EMPTY = 0
};

static uint64_t calculate_padding(const uint64_t image_width) {
    return image_width % 4;
}

static uint64_t calculate_image_size(const struct image *image) {
    const uint64_t width = image->width;
    const uint64_t height = image->height;
    const uint64_t padding = calculate_padding(width);
    return width * height * PIXEL_SIZE + width * height * padding;
}

static struct bmp_header create_header(const struct image *image) {
    const uint64_t image_size = calculate_image_size(image);
    return (struct bmp_header) {
            .bfType = SIGNATURE,
            .bfileSize = HEADER_SIZE + image_size,
            .bfReserved = EMPTY,
            .bOffBits = OFF_BITS,
            .biSize = INFO_SIZE,
            .biWidth = image->width,
            .biHeight = image->height,
            .biPlanes = PLANES,
            .biBitCount = BITS,
            .biCompression = COMPRESSION,
            .biSizeImage = image_size,
            .biXPelsPerMeter = PIX_PER_METER,
            .biYPelsPerMeter = PIX_PER_METER,
            .biClrUsed = ALL_COLORS,
            .biClrImportant = ALL_COLORS
    };
}

enum status from_bmp(FILE *stream, struct image *image) {
    struct bmp_header bmp_header = {0};

    if (stream == NULL || image == NULL || !fread(&bmp_header, HEADER_SIZE, 1, stream)) return READ_OPEN_ERROR;
    if (bmp_header.bfType != SIGNATURE) return READ_INVALID_SIGNATURE;
    if (bmp_header.bOffBits != OFF_BITS) return READ_INVALID_OFF_BITS;
    if (bmp_header.biSize != INFO_SIZE) return READ_INVALID_INFO_SIZE;
    if (bmp_header.biHeight <= 0 || bmp_header.biWidth <= 0) return READ_INVALID_WIDTH_HEIGHT;
    if (bmp_header.biPlanes != PLANES) return READ_INVALID_PLANES;
    if (bmp_header.biBitCount != BITS) return READ_INVALID_BITS;
    if (bmp_header.biCompression != COMPRESSION) return READ_INVALID_COMPRESSION;
    if (bmp_header.biClrUsed != ALL_COLORS || bmp_header.biClrImportant != ALL_COLORS) return READ_INVALID_COLORS;

    const uint64_t image_width = bmp_header.biWidth;
    const uint64_t image_height = bmp_header.biHeight;
    *image = create_image(image_width, image_height);
    if (image->data == NULL) return READ_NULL_ERROR;

    struct pixel pixel = {0};
    const int64_t padding = (int64_t) calculate_padding(image_width);

    for (size_t i = 0; i < image_height; i++) {
        for (size_t j = 0; j < image_width; j++) {
            if (!fread(&pixel, PIXEL_SIZE, 1, stream)) return READ_PIXELS_ERROR;
            image->data[j + i * image_width] = pixel;
        }
        fseek(stream, padding, SEEK_CUR);
    }

    return READ_OK;
}

enum status to_bmp(FILE *stream, const struct image *image) {
    if (stream == NULL || image == NULL) return WRITE_ERROR;

    const struct bmp_header bmp_header = create_header(image);
    if (!fwrite(&bmp_header, HEADER_SIZE, 1, stream)) return WRITE_ERROR;

    struct pixel pixel = {0};
    const uint64_t image_width = image->width;
    const uint64_t image_height = image->height;
    const int64_t padding = (int64_t) calculate_padding(image_width);
    const size_t ptr = 0;

    for (size_t i = 0; i < image_height; i++) {
        for (size_t j = 0; j < image_width; j++) {
            pixel = image->data[j + i * image_width];
            if (!fwrite(&pixel, PIXEL_SIZE, 1, stream)) return WRITE_ERROR;
        }
        if (!fwrite(&ptr, padding, 1, stream)) return WRITE_ERROR;
    }

    return WRITE_OK;
}
