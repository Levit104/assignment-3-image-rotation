#include "file.h"

enum status open_file(const char *filename, FILE **stream, const char *mode) {
    if (filename == NULL || (*stream = fopen(filename, mode)) == NULL) {
        return IO_OPEN_ERROR;
    }
    return IO_OPEN_OK;
}

enum status close_file(FILE *stream) {
    if (stream == NULL || fclose(stream) == EOF) {
        return IO_CLOSE_ERROR;
    }
    return IO_CLOSE_OK;
}
