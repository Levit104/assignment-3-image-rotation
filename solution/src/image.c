#include "image.h"
#include <stdlib.h>

struct image create_image(const uint64_t width, const uint64_t height) {
    return (struct image) {
            .width = width,
            .height = height,
            .data = malloc(width * height * PIXEL_SIZE)
    };
}

void free_image(struct image image) {
    free(image.data);
    image.width = 0;
    image.height = 0;
}
