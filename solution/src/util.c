#include "util.h"
#include <stdio.h>

void println(const char *str) {
    fprintf(stdout, "%s\n", str);
}

void println_error(const char *str) {
    fprintf(stderr, "%s\n", str);
}
